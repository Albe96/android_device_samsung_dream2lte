# Kernel
TARGET_KERNEL_CONFIG := lineage_dream2lte_defconfig

# Recovery
TARGET_OTA_ASSERT_DEVICE := dream2lte,dreamlte,dream2ltexx

# Inherit common board flags
include device/samsung/dream-common/BoardConfigCommon.mk
